package com.dongluhitec.card.deviceUpdeter.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductManager {

	private static Logger LOGGER = LoggerFactory.getLogger(ProductManager.class);

	public static String get(String mac_key) {
		if(mac_key == null){
			return "未知";
		}
		String mac_name;
		switch (mac_key) {
			case "b4-aa":
				mac_name = "单门控制器";
				break;
			case "b4-bb":
				mac_name = "双门单项控制器";
				break;
			case "b4-cc":
				mac_name = "双门双向控制器";
				break;
			case "b4-dd":
				mac_name = "四门控制器";
				break;
			case "b4-ee":
				mac_name = "窗口扣费POS机";
				break;
			case "b4-ff":
				mac_name = "小票扣费POS机";
				break;
			case "b4-11":
				mac_name = "IC卡电梯控制器";
				break;
			case "b4-22":
				mac_name = "闸机门禁控制器";
				break;
			case "b4-33":
				mac_name = "停车场4000";
				break;
			case "b4-44":
				mac_name = "停车场2000";
				break;
			case "b4-55":
				mac_name = "双层票箱";
				break;
			default:
				mac_name = "未知";
				break;
		}
		LOGGER.debug("设备信息:{} = {}", mac_key, mac_name);
		return mac_name;
	}

}
