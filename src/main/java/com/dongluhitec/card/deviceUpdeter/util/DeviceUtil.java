package com.dongluhitec.card.deviceUpdeter.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by xiaopan on 2016-04-25.
 * 设备操作
 */
public class DeviceUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceUtil.class);

    final byte[] sendMessage = {0x01, 0x57, 0x00, 0x01, 0x00, 0x01, 0x04, 0x02, 0x44, 0x4C, 0X32, 0X30, 0X31, 0X35, 0X30, 0X31, 0X32, 0X30, 0X03, 0X00};

    public void enterIAP(String deviceIP) {
        try (Socket socket = new Socket(deviceIP, 10001)) {
            OutputStream outputStream = socket.getOutputStream();
            outputStream.write(sendMessage);
            outputStream.flush();
            LOGGER.info("己对设备:{} 发送 iap 指令", deviceIP);
        } catch (Exception e) {
            LOGGER.error("对设备:" + deviceIP + " 发送 iap 指令是发生错误", deviceIP, e);
        }
    }

}